import "./App.css";
import MyNavBar from "./components/MyNavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-toastify/dist/ReactToastify.css';
import UserCard from "./components/UserCard";
import { BrowserRouter as BigRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import NewUser from "./pages/NewUser";
import AllUsers from "./pages/AllUsers";
function App() {
  return (
    <div>
      <BigRouter>
        <MyNavBar/>
        <Routes>
          <Route path="" index element={<HomePage/>}/>
          <Route path="allusers" index element={<AllUsers/>}/>
          <Route path="newuser" index element={<NewUser/>}/>
        </Routes>
      </BigRouter>
    </div>
  );
}
export default App;
