import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function UserCard(props) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={props.user.avatar} 
        onError={({ currentTarget }) => {
          currentTarget.onerror = null; // prevents looping
          currentTarget.src="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png";
        }}
      />
      <Card.Body>
        <Card.Title>{props.user.name}</Card.Title>
        <Card.Text>
        {props.user.email}
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default UserCard;