import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import UserCard from "../components/UserCard";
import { GET_ALL_USERS } from "../service/userService";
import { RotatingLines } from "react-loader-spinner";
const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    GET_ALL_USERS()
      .then((response) => {
        setUsers(response);
        setIsLoading(false);
      })
      .catch((err) => console.log("ERROR : ", err));
  }, []);
  console.log("ALL USERS : ", users);
  return (
    <div className="container">
      <h1>AllUsers</h1>

      {isLoading ? (
        <div className="d-flex justify-content-center">
          <RotatingLines
            strokeColor="grey"
            strokeWidth="5"
            animationDuration="0.75"
            width="96"
            visible={true}
          />
        </div>
      ) : (
        <div className="row">
          {users.map((user) => (
            <div className="col-4 d-flex justify-content-center">
              <UserCard user={user} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default AllUsers;
