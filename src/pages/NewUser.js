import React, { useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import { CREATE_USER } from "../service/userService";
import { ThreeDots } from "react-loader-spinner";
const NewUser = () => {
  // const [user, setUser]= useState({})
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [avatar, setAvatar] = useState("");

  let newUser = {
    name: name,
    email: email,
    password: password,
    role: role,
    avatar: avatar,
  };

  console.log("New User is : ", newUser);

  const handleCreateUser = () => {
    setIsLoading(true);
    CREATE_USER(newUser)
      .then((response) => {
        console.log("TESTING:", response);
        setIsLoading(false);
        toast("Successfully created a new user ");
      })
      .catch((error) => {
        console.log("ERORR", error);
        toast(error.response.data.message[0]);
        setIsLoading(false)
      });
  };

  const handleClear = () => {
    toast("Button clear is clicked ");
    setName("");
    setEmail("");
    setAvatar("");
    setRole("");
    setPassword("");
  };

  let buttonStatus = name && email && password && role && avatar ? false : true;
  console.log("Button Status : ", buttonStatus);
  return (
    <div className="container ">
      <div className="wrapper d-flex bg-light rounded-3 mt-4  ">
        <div
          className="image-side w-50  d-flex 
        
        justify-content-center flex-column align-items-center"
        >
          <img
            className="object-contain-fit"
            width="400px"
            src="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png"
            alt="user placeholder"
          />
          <input type="file" className="form-control w-50  " />
        </div>

        <div className="input-side  mt-4  w-50 pe-5">
          <h2> User Information </h2>

          <div className="d-flex">
            <div>
              <label htmlFor="">Username </label>
              <input
                className="form-control"
                type="text"
                placeholder="Enter name "
                name=""
                id="username"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="ms-4">
              <label htmlFor="">Role </label>
              <input
                className="form-control"
                type="text"
                placeholder="admin or customer "
                name=""
                id="role"
                value={role}
                onChange={(e) => setRole(e.target.value)}
              />
            </div>
          </div>

          <div className="d-flex">
            <div>
              <label htmlFor="">Email </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter email "
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="ms-4">
              <label htmlFor="">Password </label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password "
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
          </div>

          <label htmlFor="">Avatar Links </label>
          <input
            type="text"
            className="form-control"
            placeholder="Enter avatar links "
            onChange={(e) => setAvatar(e.target.value)}
            value={avatar}
          />

          <div
            className="d-flex justify-content-between
          align-items-start
          "
          >
            <button
              className="btn btn-warning mt-3"
              disabled={buttonStatus}
              onClick={handleCreateUser}
            >
              {" "}
              Create User{" "}
            </button>

            {isLoading ? (
              <ThreeDots
                height="80"
                width="80"
                radius="9"
                color="#4fa94d"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            ) : (
              <></>
            )}

            <button className="btn btn-danger mt-3 ms-5" onClick={handleClear}>
              {" "}
              Clear
            </button>
          </div>

          <ToastContainer />
        </div>
      </div>
    </div>
  );
};

export default NewUser;
